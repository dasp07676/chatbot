const express = require("express");
const path = require("path");
const http = require("http");
const socketio = require("socket.io");
const formatMessage = require('./utils/messages');
const {userJoin, getCurrentUser, userLeave, getRoomUsers} = require('./utils/users');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

const PORT = 3000;

// Set static folder
const staticPath = path.join(__dirname, 'public');
// console.log(staticPath);
app.use(express.static(staticPath));

const bot = 'Bot';

// When a client enters
io.on("connection", socket => {

    // Listen for chatMessage
    socket.on('chatMessage', (msg) => {
        const user = getCurrentUser(socket.id);
        io.to(user.room).emit('message', formatMessage(user.username, msg));
    })

    // Listen user messages
    socket.on('joinRoom', ({ username, room }) => {
        const user = userJoin(socket.id, username, room);

        socket.join(user.room);

        // Welcome the current user
        socket.emit('message', formatMessage(bot, 'Welcome to ChatBot'));

        // When a user connects we broadcast it to others in chat room
        // Broadcast to all the clients except the client which is connecting
        socket.broadcast.to(user.room).emit('message', formatMessage(bot, `${user.username} has joined the chat`));

        // Send users and room info
        io.to(user.room).emit('roomUsers', {
            room: user.room,
            users: getRoomUsers(user.room)
        })
    })

    // When the user disconnetcs
    socket.on('disconnect', () => {
        const user = userLeave(socket.id);
        if(user) {
            io.to(user.room).emit('message', formatMessage(bot, `${user.username} has left the chat`));
        }

        // Send users and room info
        io.to(user.room).emit('roomUsers', {
            room: user.room,
            users: getRoomUsers(user.room)
        })
    })
})

server.listen(PORT, () => {
    console.log("Server running!!!");
})