const chat = document.getElementById('chat-form');
const msg = document.getElementById('msg');
const chatMessages = document.querySelector('.chat-messages');
const roomName = document.getElementById('room-name');
const userList = document.getElementById('users');

// Get username and rooms from the url
const { username, room } = Qs.parse(location.search, {
    ignoreQueryPrefix: true
})

// console.log(username, room);

const socket = io();

// Join chat room
socket.emit('joinRoom', { username, room });

// Get room and users
socket.on('roomUsers', ({ room, users }) => {
    outputRoomName(room);
    outputUsers(users);
})

// Message from server
socket.on('message', message => {
    console.log(message);
    output(message);

    // Scroll down to the end
    chatMessages.scrollTop = chatMessages.scrollHeight;
})

// On message submit
chat.addEventListener('submit', (e) => {
    e.preventDefault();

    // Emit message to server
    socket.emit('chatMessage', msg.value);
    msg.value = "";
    msg.focus();
})

// Output message to chat window
function output(msg) {
    const newDiv = document.createElement('div');
    newDiv.classList.add('message');
    newDiv.innerHTML = `<p class="meta">${msg.username} <span>${msg.time}</span></p>
   <p class="text">${msg.text}</p>`;
    document.querySelector('.chat-messages').appendChild(newDiv);
}

function outputRoomName(room) {
    roomName.innerText = room;
}

function outputUsers(users) {
    userList.innerHTML = `
        ${users.map(user => `<li>${user.username}</li>`).join('')}
    `;
}